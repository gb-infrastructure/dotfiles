syntax on

set tabstop=4
set shiftwidth=4
set expandtab

set number
set relativenumber

set list
set listchars=tab:>-,trail:~

set autoread

filetype plugin indent on

call plug#begin('~/.vim/plugged')
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'vim-ctrlspace/vim-ctrlspace'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
call plug#end()

set hidden
set showcmd

let mapleader = ","
function! FormatFile()
    let l:lines = "all"
    py3f /usr/share/clang/clang-format.py
endfunction

nnoremap E :wincmd v <bar> :E <bar> :vertical resize 30 <CR>
nnoremap <silent> <F6> :let _s=@/ <bar> :%s/\s\+$//e <bar> :let @/=_s <bar> :nohl <bar> :unlet _s <CR>

" Highlight the symbol and it's references
autocmd CursorHold * silent call CocActionAsync('highlight')

nnoremap <silent> gd <Plug>(coc-definition)
nnoremap <silent> gy <Plug>(coc-type-definition)
nnoremap <silent> gi <Plug>(coc-implementation)
nnoremap <silent> gr <Plug>(coc-references)
nnoremap <silent> gs :CocCommand clangd.switchSourceHeader <CR>

nnoremap <leader>p :GFiles <CR>
nnoremap <leader>P :Files <CR>
nnoremap <leader>cf :call FormatFile() <CR>
nnoremap <leader>rn <Plug>(coc-rename)

nnoremap <leader>h :wincmd h<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>l :wincmd l<CR>

" map <C-K> :pyf /usr/share/clang/clang-format.py <CR>
" imap <C-K> <c-o> :pyf /usr/share/clang/clang-format.py <CR>

nnoremap <A-j> :m .+1<CR>==

set updatetime=500
